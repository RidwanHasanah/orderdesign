
$(document).ready(function(){
	function checkVal(//function for check input
		name, //long string check 
		value, //value string for show
		error,//<span> id in the html for the show();
		condition //Kondisi if
		){
		if (name <= condition) {
			error.html(value).show(); //menampilkan text string yang ada
			error.show();//menampilkan span
		}else{
			error.hide(); //jika false maka hide
		}
	}

	$('#name').focusout(function(){
		checkVal($('#name').val().length,'Nama harus lebih dari 2 karakter',$('#errorName'),2)
	})
	$('#email').focusout(function(){
		checkVal($('#email').val().length,'Email harus lebih dari 5 karakter',$('#errorEmail'),5)
	})
	$('#hp').focusout(function(){
		checkVal($('#hp').val().length,'Nomor hp harus lebih dari 10 karakter',$('#errorHp'),10)
	})
	$('#address').focusout(function(){
		checkVal($('#address').val().length,'Alamat harus lebih dari 10 karakter',$('#errorAddress'),10)
	})
	$('#message').focusout(function(){
		checkVal($('#message').val().length,'Pesan harus lebih dari 10 karakter',$('#errorMessage'),10)
	})
	$('#hope').focusout(function(){
		checkVal($('#hope').val().length,'Tanggal harus di isi sesuai format',$('#errorHope'),6)
	})
})

// $(function(){
// 	$.validator.setDefaults({
// 		highlight: function(element){
// 			$(element)
// 			.closest('.form-group')
// 			.addClass('has-error')
// 		},
// 		unhighlight: function(element){
// 			$(element)
// 			.closest('.form-group')
// 			.removeClass('has-error')
// 		}
// 	});
	
// 	$.validate({
// 		rules:
// 		{	
// 		    password: "required",
// 			birthDate: "required",
// 			weight: {
// 			    required:true,
// 			    number:true
// 			},
// 			height:  {
// 			    required:true,
// 			    number:true
// 			},
// 			email: {
// 				required: true,
// 				email: true
// 			}
// 		},
// 			messages:{			
// 				email: {
// 				required: true,
// 				email: true
// 			}
// 		},
// 				password: {
// 					required: " Please enter password"
// 				},
// 				birthDate: {
// 					required: " Please enter birthdate"
// 				},
// 				email: {
// 					required: ' Please enter email',
// 					email: ' Please enter valid email'
// 				},
// 				weight: {
// 					required: " Please enter your weight",
// 					number: " Only numbers allowed"
// 				},
// 				height: {
// 					required: " Please enter your height",
// 					number: " Only numbers allowed"
// 				},
// 			}
			
// 	});
// });