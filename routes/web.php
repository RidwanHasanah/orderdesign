<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
    return view('order');
});

Route::resource('/order','OrderController');

Auth::routes();

Route::get('login/google', 'Auth\LoginController@redirectToGoogle')->name('login.google');
Route::get('login/google/callback', 'Auth\LoginController@handleGoogleCallback');

Route::group(['middleware'=>['auth']], function(){  
	Route::get('/home', 'HomeController@index')->name('home');
	Route::resource('/admin','Admin\AdminOrderController');
	Route::get('apiorder', 'Admin\AdminOrderController@apiOrder')->name('api.order');
});