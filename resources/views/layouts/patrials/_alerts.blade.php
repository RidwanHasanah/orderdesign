@if (session('success'))
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<i class="material-icons">close</i>
		</button>
		<span style="text-align: center;">
			<b>{{ session('success') }}</b></span>
	</div>
@endif

@if (session('info'))
  	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<i class="material-icons">close</i>
		</button>
		<span style="text-align: center;">
			<b>{{ session('info') }}</b></span>
	</div>
@endif

@if (session('danger'))
  <div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<i class="material-icons">close</i>
		</button>
		<span style="text-align: center;">
			<b>{{ session('danger') }}</b></span>
	</div>
@endif
