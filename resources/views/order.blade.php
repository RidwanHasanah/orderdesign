<!DOCTYPE html>
<html>
<head>
    <title>Order Desain</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    {{-- <script src="//code.jquery.com/jquery-2.1.3.min.js"></script> --}}
    <!------ Include the above in your HEAD tag ---------->
    <link rel="stylesheet" type="text/css" href="{{asset('css/order.css')}}">
    {{-- <script src="{{asset('js/order.js')}}"></script> --}}
    <script src="{{asset('js/date.js')}}"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    {{-- CSS DASHBOARD --}}
    
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{asset('assets/css/material-dashboard.css?v=2.0.0')}}">
    <!-- Documentation extras -->
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{asset('assets/assets-for-demo/demo.css" rel="stylesheet')}}" />
    <!-- iframe removal -->
</head>
<body>
    <br>
    <div>@include('layouts.patrials._alerts')</div>
    <br>
    <div class="container" id="order-form">
        
            <form action="{{route('order.store')}}" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <h2>Order Desain</h2>
                <div class="form-group">
                    <label for="name" class="col-sm-3 control-label">Nama Lengkap</label>
                    <div class="col-sm-9">
                        <input required  type="text" id="name" name="name" placeholder="Nama" class="form-control" autofocus>
                        <span  class="errorval errorRegis" id="errorName"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input required  type="email" name="email" id="email" placeholder="example@sample.com" class="form-control" autofocus>
                        <span  class="errorval errorRegis" id="errorEmail"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="phoneNumber" class="col-sm-3 control-label">Nomor Hp/WA </label>
                    <div class="col-sm-9">
                        <input required type="number" id="hp" placeholder="Phone number" name="hp" class="form-control">
                        <span  class="errorval errorRegis" id="errorHp"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="fb" class="col-sm-3 control-label">Facebook</label>
                    <div class="col-sm-9">
                        <input type="link" id="fb" placeholder="Facebook" name="fb" class="form-control">
                        <span  class="errorval errorRegis" id="errorFb"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-3 control-label">Alamat Lengkap</label>
                    <div class="col-sm-9">
                        <textarea required class="form-control" name="address" id="address"></textarea>
                        <span  class="errorval errorRegis" id="errorAddress"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="message" class="col-sm-3 control-label">Pesan / Keperluan</label>
                    <div class="col-sm-9">
                        <textarea rows="20" required class="form-control" name="message" id="message" placeholder="Contoh order untuk loker
                        Info Lowongan Kerja

Pondok IT adalah sebuah suatu tempat pendidikan IT yang berada di Yogyakarta yang selain membekali santrinya dengan pengetahuan Teknologi Informasi dan Komputer (TIK), juga membekali mereka dengan ilmu agama. 

Dibutuhkan 2 Staf Manajemen di Pondok IT

📝 Kriteria
1. Muslim Taat
2. Menyukai dunia IT /Lingkungan pondok
3. Jujur, Disiplin, Bertanggung Jawab
4. Min Lulusan SMK/SMA/MA
5. Tidak Merokok
6. Diutamakan Belum Nikah
7. Usia Max 27 Tahun
8. Semangat Dalam Belajar / Mau Mengembangkan Kemampuan
9. Bersedia dibimbing dan diarahkan

🎁 Fasilitas
1. Gaji Pokok
2. Makan 3 kali sehari. 
3. Tempat Tinggal
4. Free Wifi di lingkungan pondok. 
5. Kajian Islam
6. Pinjaman Laptop

🗂 Berkas Lamaran
1. Surat lamaran pekerjaan. 
2. Daftar Riwayat Hidup (CV). 
3. Fotocopy ijazah terakhir.
4. Sertifikat lain yg mendukung. 
5. Fotocopy KTP.
6. Foto berwarna 4x6

Berkas lamaran dikirimkan dalam format PDF ke WA 

ℹ Informasi
Hubungi Rulli (Wa/Telepon) : 085228802828 

📭 *Alamat Pondok Programmer(Pondok IT), Glagah lor, No 64 RT 02, Desa tamanan, kecamatan banguntapan, Bantul, Yogyakarta 55191 www.PondokIT.com
www.pondokprogrammer.com"></textarea>
                        <span  class="errorval errorRegis" id="errorMessage"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ref" class="col-sm-3 control-label">Refrensi</label>
                    <div class="col-sm-9">
                        <input style="height: 40px;" type="file" name="ref" id="ref" class="form-control">
                        <span  class="errorval errorRegis" id="errorRef"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="queue" class="col-sm-3 control-label">Siap Mengantri</label>
                    <div class="col-sm-9">
                        <select style="height: 35px;" class="form-control" id="queue" name="queue">
                            <option value="Ya" class="form-control">Ya</option>
                            <option value="Tidak" class="form-control">Tidak</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="hope" class="col-sm-3 control-label">Harapan Selesai</label>
                    <div class="col-sm-9">
                        <input required type="date" id="datepicker" name="hope" class="form-control">
                        <span  class="errorval errorRegis" id="errorHope"></span>
                    </div>
                </div>
                 <!-- /.form-group -->
                <button id="submit" type="submit" class="btn btn-primary btn-block">Kirim</button>
            </form> <!-- /form -->
        </div> <!-- ./container -->
</body>
<script src="{{asset('js/order.js')}}"></script>

</html>


