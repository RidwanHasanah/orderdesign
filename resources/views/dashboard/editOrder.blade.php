@extends('dashboard.masterDashboard')
@section('title')
Edit Orderan
@endsection
@section('content')
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				@include('layouts.patrials._alerts')
				<h1 class="page-header"> Edit Orderan</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						<h3 class="panel-title">Edit</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<form action="{{route('admin.update', $order->id)}}" method="post" role="form" enctype="multipart/form-data">
                    {{csrf_field()}}
                    {{ method_field('PATCH') }}
                <div class=" col-md-12 col-lg-12 "> 
                 
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td><b>Nama</b></td>
                        <td><input placeholder="Ridwan" class="form-control" type="" name="name" value="{{$order->name}}"></td>
                      </tr>
                      <tr>
                        <td><b>Email</b></td>
                        <td><input readonly class="form-control" type="email" name="email" value="{{$order->email}}"></td>
                      </tr>
                      <tr>
                        <td><b>Nomor Hp/WA</b></td>
                        <td><input class="form-control"" name="hp" value="{{$order->hp}}"></td>
                      </tr>
                      <tr>
                        <td><b>Facebook</b></td>
                        <td><input class="form-control" type="link" name="fb" value="{{$order->hp}}"></td>
                      </tr>
                      <tr>
                        <td><b>Alamat</b></td>
                        <td><textarea placeholder="Contoh : jl.pesangrahan" class="form-control" type="text" name="address">{{$order->address}}</textarea></td>
                      </tr>
                      <tr>
                        <td><b>Pesan / Keperluan</b></td>
                        <td><textarea class="form-control" type="text" name="message">{{$order->message}}</textarea></td>
                      </tr>
                      <tr>
                        <td><b>Status</b></td>
                        <td>
                          <select name="status" class="form-control">
                            <option {{$order->status=='Waiting' ? 'selected' : ''}} value="Waiting">Waiting</option>
                            <option {{$order->status=='Pending' ? 'selected' : ''}} value="Pending">Pending</option>
                            <option {{$order->status=='Completed' ? 'selected' : ''}} value="Completed">Completed</option>
                            
                            <option {{$order->status=='In Proses' ? 'selected' : ''}} value="In Proses">In Proses</option>
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td><b>Reffrensi</b></td>
                        <td><a href="{{$order->ref}}">Download</a></td>
                      </tr>
                      <tr>
                        <td><b>Siap Mengantri</b></td>
                        <td>
                        	<select name="queue" id="queue">
                        		<option value="Ya" {{$order->queue=="YA"?'selected':''}}>Ya</option>
                        		<option value="Tidak" {{$order->queue=="Tidak"?'selected':''}}>Tidak</option>
                        	</select>
                        </td>
                      </tr>
                      <tr>
                        <td><b>Harapan selesai</b></td>
                        <td><input id="datepicker" class="form-control" type="" name="hope" value="{{$order->hope}}"></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td><input class="btn btn-primary" type="submit" name="submit"></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                </form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
@endsection