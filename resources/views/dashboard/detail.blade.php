@extends('dashboard.masterDashboard')
@section('title')
Orderan Detail
@endsection
@section('content')
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Orderan</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						<h3 class="panel-title">Orderan Detail</h3>
					</div>
					<div class="panel-body">
						<div class="row">
                <div class=" col-md-12 col-lg-12 "> 
                 
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td><b>Nama</b></td>
                        <td>{{$order->name}}</td>
                      </tr>
                      <tr>
                        <td><b>Email</b></td>
                        <td>{{$order->email}}</td>
                      </tr>
                      <tr>
                        <td><b>Nomor Hp/WA</b></td>
                        <td>{{$order->hp}}</td>
                      </tr>
                      <tr>
                        <td><b>Facebook</b></td>
                        <td>{{$order->hp}}</td>
                      </tr>
                      <tr>
                        <td><b>Alamat</b></td>
                        <td>{{$order->address}}</td>
                      </tr>
                      <tr>
                        <td><b>Pesan / Keperluan</b></td>
                        <td>{{$order->message}}</td>
                      </tr>
                      <tr>
                        <td><b>Reffrensi</b></td>
                        <td><a href="{{asset('uploads/file/'.$order->ref) }}">Download</a></td>
                      </tr>
                      <tr>
                        <td><b>Siap Mengantri</b></td>
                        <td>{{$order->queue}}</td>
                      </tr>
                      <tr>
                        <td><b>Harapan selesai</b></td>
                        <td>{{$order->hope}}</td>
                      </tr>
                      <tr>
                        <td><b>Status</b></td>
                        <td>{{$order->status}}</td>
                      </tr>
                    </tbody>
                    <tr>
                      <td><a class="btn btn-info" href="{{route('admin.edit',$order->id)}}">Edit</a></td>
                      <td></td>
                      
                    </tr>
                  </table>
                </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
@endsection