@extends('dashboard.masterDashboard')
@section('title')
Orderan
@endsection
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Semua Orderan</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Semua Orderan
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="order-table">
                        <thead>
                            <tr>
                                {{-- <th>No</th> --}}
                                <th >Nama</th>
                                <th >Pesan</th>
                                <th>Status</th>
                                <th>Dead line</th>
                                <th width="110">Action</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	var table = $('#order-table').DataTable({
		order: [[0,'desc']],
		processing: true,
		serverSide: true,
		ajax: "{{route('api.order')}}",
		columns:[
			// {data: 'no', name: 'no'},
			{data: 'name', name: 'name'},
			{data: 'message', name: 'message'},
			{data: 'status', name: 'status'},
      {data: 'hope', name: 'hope'},
			{data: 'action', name: 'action', ordertable: false, searchable:false}
		]
	})

	/*Delete Data*/
      function deleteOrder(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');

        swal({
          title: 'Kamu yakin ?',
          text: "Data Order yang di hapus tidak akan kembali!",
          type: 'warning',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Ya, Hapus ini!'
        }).then(function(){
          $.ajax({
            url: "{{ url('admin') }}" + '/' + id,
            type: "DELETE",
            data: {'_mehtod' : 'DELETE', '_token' : csrf_token},
            success: function(data){
              table.ajax.reload();
              swal({
                title:'Berhasil',
                text: 'Orderan Sudah di hapus',
                type: 'success',
                timer: '1500'
              })
            },
            error: function(){
              swal({
                title: 'Oops',
                text: 'Something went wrong',
                type: 'error',
                timer: '1500'
              })
            }
          })
        })
      }

     function editOrder(id){
     	var url =  "{{ url('admin') }}"+"/"+id+"/edit";
     	window.location = url;
     }

     function showOrder(id){
     	window.location = "{{url('admin')}}"+"/"+id;
     }
</script>
@endsection