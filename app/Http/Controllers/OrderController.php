<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class OrderController extends Controller
{
    public function create()
    {
        return view('order');
    }
    public function store(Request $request)
    {
        // Order::create([
        //     'name'    => request('name'),
        //     'email'   => request('email'),
        //     'hp'      => request('hp'),
        //     'fb'      => request('fb'),
        //     'address' => request('address'),
        //     'message' => request('message'),
        //     'ref'     => request('ref'),
        //     'queue'   => request('queue'),
        //     'hope'    => request('hope'),

        // ]);

        $order = new Order;//DB::table('orders');

        $order->name = $request->name;
        $order->email = $request->email;
        $order->hp = $request->hp;
        $order->fb = $request->fb;
        $order->address = $request->address;
        $order->message = $request->message;
        $order->queue = $request->queue;
        $order->hope = $request->hope;

        if ($request->hasFile('ref')) {
            $file = $request->file('ref'); 
            $filename = $request->name.$file->getClientOriginalName();
            // Storage::disk('uploads')->put('file', $file);
        // dd(store('docs',$filename));

            // $request->file('ref')->storeAs('docs',$filename);
            $file->move('uploads/file/', $filename);
            $order->ref = $filename; 
        }

        $order->save();

        return redirect()->back()->with('success', 'Order anda sudah kami terima');;
    }
}
