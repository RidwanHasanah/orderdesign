<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show Google login page.
     *
     * @return link
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Accepting Google Callback. From here, all data
     * will be proceeded as a login account.
     *
     * If attempt success, user will be redirect to homepage.
     * If attempt fail, user will be redirect back to login page.
     *
     * @return link
     */
    public function handleGoogleCallback()
    {
        $user = Socialite::driver('google')->stateless()->user();
        $this->_loginUser($user);

        return redirect()->route('home');
    }
    protected function _loginUser($data)
    {
        $user = User::where('email', '=', $data->email)->first();
        if (!$user) {
            return redirect('login')->with('email-error', 'Akun Google anda tidak terdaftar dalam sistem');
        }

        Auth::login($user);
    }
}
