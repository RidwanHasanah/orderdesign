<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Order;
use Illuminate\Support\Facades\Auth; //untukmenggunakan Controller Auth
use Illuminate\Support\Facades\DB;

class AdminOrderController extends Controller
{
    
    public function index()
    {
        return view('dashboard.order');
    }

    
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        return view('dashboard.detail', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        return view('dashboard.editOrder', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $order->update([
            'name' => request('name'),
            'email' => request('email'),
            'hp' => request('hp'),
            'fb' => request('fb'),
            'address' => request('address'),
            'message' => request('message'),
            'queue' => request('queue'),
            'hope' => request('hope'),
            'status' => request('status'),
        ]);

        return redirect()->back()->with('success','Berhasil di ubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $orders = Order::find($id);
        $orders->delete();
    }

    public function apiOrder(Request $request){

        DB::statement(DB::raw('set @rownum=0'));
        $orders = Order::select([
            DB::raw('@rownum := @rownum + 1 AS no'),
            'id',
            'name',
            'email',
            'hp',
            'fb',
            'address',
            'message',
            'ref',
            'queue',
            'hope',
            'status',
            'info',
        ])->latest('id');

        $datatables = Datatables::of($orders);

        if($keyword = $request->get('search')['value']){
            $datatables->filterColumn('no', 'whereRaw', '@rownum + 1 like ?', ["{$keyword}%"]);
        }

        return $datatables->addColumn('action', function($orders){
            return '<a onclick="showOrder('.$orders->id.')" class="btn btn-outline btn-primary btn-xs" style="margin-right:2px">Detail</a>'.
                    '<a onclick="editOrder('.$orders->id.')" class="btn btn-outline btn-success btn-xs" style="margin-right:2px">Edit</a>'.
                    '<a onclick="deleteOrder('.$orders->id.')" class="btn btn-outline btn-danger btn-xs" style="margin-right:2px">Delete</a>';

        })->make(true);
    }
}
