<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('hp');
            $table->string('fb')->nullable();
            $table->text('address');
            $table->text('message');
            $table->string('ref')->nullable();
            $table->enum('queue',[
                'Ya',
                'Tidak'
            ]);
            $table->date('hope');
            $table->enum('status',[
                'Pending',
                'Completed',
                'Waiting',
                'In Proses'
            ])->nullable();
            $table->text('info')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
